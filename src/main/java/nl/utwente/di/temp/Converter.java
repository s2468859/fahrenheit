package nl.utwente.di.temp;

import java.util.HashMap;

public class Converter {


//    HashMap<String, Double> isbnResults = new HashMap<>();
//
//    public void putResults() {
//        isbnResults.put("1", 10.0);
//        isbnResults.put("2", 45.0);
//        isbnResults.put("3", 20.0);
//        isbnResults.put("4", 35.0);
//        isbnResults.put("5", 50.0);
//        isbnResults.put("other", 0.0);
//    }
//
//    public double getBookPrice(String isbn) {
//        putResults();
//        double neededResult = isbnResults.get(isbn);
//
//        return neededResult;
//    }


    public double getTemp(String temp) {

        double input = Double.parseDouble(temp);
        double fahrenheit = ((input * 9)/5) + 32;

        return fahrenheit;
    }
}
